package types

import (
	"net/http"
	"strings"
)

type Error struct {
	code int
	err error
}

func WrapError(err error) *Error {
	if err == nil {
		return &Error{ code: http.StatusOK }
	} else if has400(err) {
		return &Error{ code: http.StatusBadRequest, err: err }
	}
	return &Error{ code: http.StatusInternalServerError, err: err }
}

func has400(err error) bool {
	str := err.Error()
	return strings.Contains(str, "not found") ||
		strings.Contains(str, "not find") ||
		strings.Contains(str, "not get") ||
		strings.Contains(str, "didn't get") ||
		strings.Contains(str, "doesn't get") ||
		strings.Contains(str, "couldn't get")
}

