package utils

var (
	SupportContentTypes = map[string]string {
		"image/gif": ".gif",
		"image/jpeg": ".jpg",
		"image/pjpeg": ".jpg",
		"image/png": ".png",
		"image/svg+xml": ".svg",
		"image/tiff": ".tiif",
		"image/vnd.microsoft.icon": ".ico",
		"image/vnd.wap.wbmp": ".wbmp",
		"image/webp": ".webp",
	}

	FileExtensions = func() map[string]string {
		var result = map[string]string{}
		for k, v := range SupportContentTypes {
			result[v] = k
		}
		return result
	}()
)
