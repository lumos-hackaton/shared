package utils

import "strings"

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

func CompareStrings(uid1, uid2 string) (string, string) {
	switch strings.Compare(uid1, uid1) {
		case 0:
			panic("uids the same: " + uid1 + ", " + uid2)
		case -1:
			return uid1, uid2
		default:
			return uid2, uid1
	}
}