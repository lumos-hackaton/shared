package structs

import (
	"strings"
	"fmt"
)

type Pageable struct {
	Start          int       `query:"start"      validate:"required,gte=0"`
	Limit          int       `query:"limit"      validate:"required,gte=1"`
	Sort           string    `query:"sort"       validate:"omitempty,oneof=ASC DESC"`
	By             string    `query:"by"         validate:"omitempty"`

	Query          string    `query:"q"          validate:"omitempty"`

	params map[string]string
}

const (
	sep = ":" // §
)

func (p *Pageable) Params() map[string]string {
	res := map[string]string{}
	p.Query = strings.TrimPrefix(strings.TrimSuffix(p.Query, "]"), "[")

	for _, q := range strings.Split(p.Query, ",") {
		qs := strings.Split(q, sep)
		fmt.Println(qs, len(qs))
		if len(qs) == 2 {
			res[qs[0]] = qs[1]
		}
	}
	p.params = res
	return res
}

func (p *Pageable) Param(name string) string {
	if p.params == nil { p.Params() }
	v, ok := p.params[name]
	if !ok { return "" }
	return v
}

