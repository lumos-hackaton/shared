package utils

func ContainsString(src []string, elem string) bool {
	for _, s := range src {
		if s == elem { return true }
	}
	return false
}

func ContainsInterface(src interface{}, elem interface{}) (b bool) {
	defer func() {
		if recover() != nil { b = false }
	}()

	for _, s := range src.([]interface{}) {
		if s == elem { return true }
	}
	return false
}

func ContainsInt(src []int, elem int) bool {
	for _, s := range src {
		if s == elem { return true }
	}
	return false
}