package configs

import (
	"gopkg.in/mgo.v2"
	"crypto/tls"
	"net"
)

var (
	DatabaseConfig = func(dbConfig *DBConfig) (*mgo.DialInfo) {
		tlsConfig := &tls.Config{}

		info := &mgo.DialInfo{
			Addrs: dbConfig.Addrs,
			Database: dbConfig.Database,
			ReplicaSetName: dbConfig.ReplicaSet,
			Source: dbConfig.Source,
			Username: dbConfig.Username,
			Password:dbConfig.Password,
		}
		info.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
			if err != nil {
				panic(err)
			}
			return conn, err
		}
		return info
	}

	DatabaseConnectionString = func(dbConfig *DBConfig) string {
		str := "mongodb://"
		if dbConfig.Username != "" {
			if dbConfig.Password != "" {
				str += dbConfig.Username + ":" + dbConfig.Password + "@"
			} else {
				str += dbConfig.Username + "@"
			}
		}
		for i, a := range dbConfig.Addrs {
			str += a
			if i + i < len(dbConfig.Addrs) {
				str += ","
			}
		}
		str += "/" + dbConfig.Database
		if dbConfig.ReplicaSet != "" {
			str += "?replicaSet=" + dbConfig.ReplicaSet
		}
		return str
	}
)