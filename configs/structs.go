package configs

import (
	"errors"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/mongo"
)

type ProjectConfiguration struct {
	Env      EnvConfig    `json:"env"`

	DataBase *DBConfig    `json:"database"`
	//Ssl      *SslConfig   `json:"ssl"`
}

type EnvConfig struct {
	Name     string       `json:"name"`
	Port     string       `json:"port"`
	Module   string       `json:"module"`
	Consul   ConsulConfig `json:"module"`

	Folder   string       `json:"folder"`
	Backend  string       `json:"backend"`
	Frontend string       `json:"frontend"`
}

type ConsulConfig struct {
	Addr    string        `json:"addr"`
}

type DBConfig struct {
	ConnectVia string     `json:"connectVia"`
	Addrs      []string   `json:"addr"`

	ReplicaSet string     `json:"replica"`
	Database   string     `json:"database"`

	Source     string     `json:"source"`
	Username   string     `json:"username"`
	Password   string     `json:"password"`
}

func (db *DBConfig) DBInstance() (dbase.Database, error) {
	if db.ConnectVia == "url" {
		return mongo.NewMongoDatabase(DatabaseConnectionString(db))
	} else if db.ConnectVia == "dialInfo" {
		return mongo.NewMongoDatabaseWithInfo(DatabaseConfig(db))
	}
	return nil, errors.New("unsupported `database.connectVia` config")
}

//type SslConfig struct {
//	CerPath    string     `json:"certificate"`
//	KeyPath    string     `json:"key"`
//}
