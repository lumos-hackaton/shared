package configs

import (
	"io/ioutil"
	"bitbucket.org/lumos-hackaton/shared/utils"
	"bitbucket.org/lumos-hackaton/shared/json"
)

var filename = "project.json"

func InitFileName(file string) {
	filename = file
}

var config *ProjectConfiguration = nil

func Instance() *ProjectConfiguration {
	if config == nil {
		config = initConfig(filename)
	}
	return config
}

func initConfig(filename string) *ProjectConfiguration {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil { utils.Must(err) }

	var proj ProjectConfiguration
	utils.Must(json.Unmarshal(bytes, &proj))

	return &proj
}