package json

import (
	"github.com/labstack/echo"
	"io/ioutil"
	"fmt"
	"strings"
	stdJson "encoding/json"
	"reflect"
	"bitbucket.org/lumos-hackaton/shared/utils"
)

type CustomBinder struct {}

func nameValueForValidateError(err error, i interface{}) (e error) {
	defer func() {
		if recover() != nil {
			e = err
		}
	}()

	str := err.Error()
	field := str[strings.Index(str, "'")+1:]
	field = field[:strings.Index(field, "'")]
	field = strings.Split(field, ".")[len(strings.Split(field, "."))-1]
	first := string([]rune(field)[0])
	key := strings.Replace(field, first, strings.ToLower(first), 1)
	value := reflect.ValueOf(i).Elem().FieldByName(field).String()
	return fmt.Errorf("invalid param '%s' value: '%v'", key, value)
}

var defaultBinder = &echo.DefaultBinder{}
func (cb *CustomBinder) Bind(i interface{}, c echo.Context) (err error) {
	req := c.Request()
	defaultBind := func(in interface{}, ctx echo.Context) error {
		err = defaultBinder.Bind(in, ctx) // bind `query`
		if err != nil { return err }
		err = ctx.Validate(in)
		if err != nil {
			return nameValueForValidateError(err, in)
		}
		return nil
	}

	if req.ContentLength == 0 {
		return defaultBind(i, c)
	}

	defer req.Body.Close()
	body, err := ioutil.ReadAll(req.Body)
	if err != nil { return err }

	defer func() {
		err := recover()
		if err != nil {
			fmt.Println(err)
			err = stdJson.Unmarshal(body, i)
			fmt.Println(err)
		}
	}()

	contentTypes := strings.Split(req.Header.Get(echo.HeaderContentType), ";")
	fmt.Println(contentTypes)

	if utils.ContainsString(contentTypes, "application/json") {
		err = Unmarshal(body, i)
		if err != nil {
			err = stdJson.Unmarshal(body, i)
			if err != nil {
				return err
			}
		}
	} else {
		return defaultBind(i, c)
	}

	if err = c.Validate(i); err != nil {
		return nameValueForValidateError(err, i)
	}
	return nil
}

func NewBinder() *CustomBinder {
	return &CustomBinder{}
}