package json

import (
	"github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type (
	marshaller func(interface{}) ([]byte, error)
	unmarshaller func([]byte, interface{}) error
)

var (
	defaultMarshal marshaller = json.Marshal
	defaultUnmarshal unmarshaller = json.Unmarshal
)

func Marshal(i interface{}) ([]byte, error) {
	return json.Marshal(i)
}

func MarshalToString(i interface{}) (string, error) {
	return json.MarshalToString(i)
}

func MarshalHard(i interface{}) []byte {
	r, err := Marshal(i)
	if err != nil { panic(err) }
	return r
}

func MarshallHardToString(i interface{}) string {
	r, err := MarshalToString(i)
	if err != nil { panic(err) }
	return r
}




func Unmarshal(source []byte, i interface{}) error {
	return json.Unmarshal(source, i)
}

func UnmarshalFromString(source string, i interface{}) error {
	return json.UnmarshalFromString(source, i)
}

func UnmarshalHard(source []byte, i interface{}) {
	err := Unmarshal(source, i)
	if err != nil { panic(err) }
}