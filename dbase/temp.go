package dbase

import (
	"gopkg.in/mgo.v2"
	"errors"
)

type tempDb struct {}

var (
	notImplemented = errors.New("not implemented")
)


func (*tempDb) Collection(name string) Collection {
	return &tempCollection{ name: name }
}

func (*tempDb) Collections() ([]string, error) { return make([]string, 0), errors.New("not implemented") }
func (*tempDb) CreateCollection(string) error {	return notImplemented }
func (*tempDb) Mongo() *mgo.Database { return nil }
func (*tempDb) OneRef(*DBRef, interface{}) error { return notImplemented }
func (*tempDb) ManyRefs([]*DBRef, interface{}) error { return notImplemented }



type tempCollection struct {
	name string
}

func (c *tempCollection) Name() string {
	return c.name
}
func (c *tempCollection) DBRef(id interface{}) *DBRef {
	return &DBRef{ Collection: c.name, Id: id }
}

func (*tempCollection) Find(args interface{}) Query { return nil }
func (*tempCollection) FindId(query interface{}) Query { return nil }
func (*tempCollection) Insert(docs ...interface{}) error { return notImplemented }
func (*tempCollection) Update(selector interface{}, update interface{}) error { return notImplemented }
func (*tempCollection) UpdateId(id interface{}, update interface{}) error { return notImplemented }
func (*tempCollection) UpdateAll(selector interface{}, update interface{}) error { return notImplemented }
func (*tempCollection) Remove(selector interface{}) error { return notImplemented }
func (*tempCollection) RemoveId(id interface{}) error { return notImplemented }
func (*tempCollection) DropCollection() error { return notImplemented }
func (*tempCollection) Mongo() *mgo.Collection { return nil }
