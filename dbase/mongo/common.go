package mongo

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"gopkg.in/mgo.v2"
	"strings"
)

type tempDbase struct {
	db dbase.Database
}

func NewMongoDatabaseWithInfo(info *mgo.DialInfo) (dbase.Database, error) {
	sess, err := mgo.DialWithInfo(info)
	if err != nil { return nil, err }
	return newMongo(sess.DB(info.Database)), nil
}

func NewMongoDatabase(url string) (dbase.Database, error) {
	sess, err := mgo.Dial(url)
	if err != nil { return nil, err }
	pathes := strings.Split(url, "/")
	db := strings.Split(pathes[len(pathes) - 1], "?")[0]

	return newMongo(sess.DB(db)), nil
}

func newMongo(db *mgo.Database) dbase.Database {
	return &mgoDatabase{ db: db }
}
