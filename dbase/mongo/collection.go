package mongo

import (
	"gopkg.in/mgo.v2"
	"bitbucket.org/lumos-hackaton/shared/dbase"
)

type mgoCollection struct {
	collection *mgo.Collection
}

func (c *mgoCollection) Name() string {
	return c.collection.Name
}

func (c *mgoCollection) DBRef(id interface{}) *dbase.DBRef {
	return &dbase.DBRef{
		Collection: c.Name(),
		Id: id,
	}
}

func (c *mgoCollection) Find(query interface{}) dbase.Query {
	return &mgoQuery{c.collection.Find(query)}
}

func (c *mgoCollection) FindId(id interface{}) dbase.Query {
	return &mgoQuery{c.collection.FindId(id)}
}

func (c *mgoCollection) Insert(docs ...interface{}) error {
	return c.collection.Insert(docs...)
}

func (c *mgoCollection) Update(selector interface{}, update interface{}) error {
	return c.collection.Update(selector, update)
}

func (c *mgoCollection) UpdateId(id interface{}, update interface{}) error {
	return c.collection.UpdateId(id, update)
}

func (c *mgoCollection) UpdateAll(selector interface{}, update interface{}) error {
	_, err := c.collection.UpdateAll(selector, update)
	return err
}

func (c *mgoCollection) Remove(selector interface{}) error {
	return c.collection.Remove(selector)
}

func (c *mgoCollection) RemoveId(id interface{}) error {
	return c.collection.RemoveId(id)
}

func (c *mgoCollection) DropCollection() error {
	return c.collection.DropCollection()
}

func (c *mgoCollection) Mongo() *mgo.Collection {
	return c.collection
}

