package mongo

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type mgoQuery struct {
	q *mgo.Query
}

func (q *mgoQuery) Skip(n int) dbase.Query {
	q.q = q.q.Skip(n)
	return q
}

func (q *mgoQuery) Limit(n int) dbase.Query {
	q.q = q.q.Limit(n)
	return q
}

func (q *mgoQuery) Select(selector []string) dbase.Query {
	adapted := bson.M{}
	for _, arg := range selector {
		adapted[arg] = 1
	}

	q.q = q.q.Select(adapted)
	return q
}

func (q *mgoQuery) Sort(fields ...string) dbase.Query {
	q.q = q.q.Sort(fields...)
	return q
}

func (q *mgoQuery) One(result interface{}) error {
	return q.q.One(result)
}

func (q *mgoQuery) All(result interface{}) error {
	return q.q.All(result)
}

func (q *mgoQuery) Count() (n int, err error) {
	return q.q.Count()
}

func (q *mgoQuery) Exists() (bool, error) {
	count, err := q.Count()
	return err == nil && count > 0, err
}
