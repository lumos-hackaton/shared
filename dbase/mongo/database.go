package mongo

import (
	"gopkg.in/mgo.v2"
	"errors"
	"bitbucket.org/lumos-hackaton/shared/dbase"
)

type mgoDatabase struct {
	db *mgo.Database
}

func (db *mgoDatabase) Mongo() *mgo.Database {
	return db.db
}

func (db *mgoDatabase) OneRef(ref *dbase.DBRef, obj interface{}) error {
	var source dbase.Database = db
	if ref.Database != "" {
		source = newMongo(db.db.Session.DB(ref.Database))
	}
	return source.Collection(ref.Collection).FindId(ref.Id).One(obj)
}

func (db *mgoDatabase) ManyRefs(refs []*dbase.DBRef, obj interface{}) error {
	if len(refs) == 0 { return errors.New("no such refs") }

	var ids []interface{}
	for _, r := range refs {
		ids = append(ids, r.Id)

		if r.Collection != refs[0].Collection {
			return errors.New("multiple collections don`t supported")
		}
	}
	return db.Collection(refs[0].Collection).FindId(dbase.M{ "$in": ids }).All(obj)
}

func (db *mgoDatabase) Collections() ([]string, error) {
	return db.db.CollectionNames()
}

func (db *mgoDatabase) Collection(name string) dbase.Collection {
	return &mgoCollection{ collection: db.db.C(name) }
}

func (db *mgoDatabase) CreateCollection(name string) error {
	return db.db.C(name).Create(&mgo.CollectionInfo{})
}