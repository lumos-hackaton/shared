package dbase

import (
	"gopkg.in/mgo.v2"
)

type Collection interface {
	Find(args interface{}) Query
	FindId(query interface{}) Query

	Insert(docs ...interface{}) error

	Update(selector interface{}, update interface{}) error
	UpdateId(id interface{}, update interface{}) error
	UpdateAll(selector interface{}, update interface{}) error

	Remove(selector interface{}) error
	RemoveId(id interface{}) error

	DropCollection() error
	Name() string

	// For specific queries, like Pipe with aggregation framework
	Mongo() *mgo.Collection
	DBRef(id interface{}) *DBRef
}