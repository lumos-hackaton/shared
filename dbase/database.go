package dbase

import (
	"gopkg.in/mgo.v2"
)

type Database interface {
	Collections() ([]string, error)
	Collection(name string) Collection
	CreateCollection(string) error

	Mongo() *mgo.Database
	OneRef(*DBRef, interface{}) error
	ManyRefs([]*DBRef, interface{}) error
}