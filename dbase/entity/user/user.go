package user

import (
	"github.com/pborman/uuid"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id  bson.ObjectId `bson:"_id"                json:"-"`
	UID string        `bson:"uid"                json:"uid"`

	Email     string `bson:"email"              json:"email"`
	Password  string `bson:"password"           json:"password"`
	Username  string `bson:"username"           json:"username"`
	Token     string `bson:"token"              json:"token"`
	Telephone string `bson:"telephone"          json:"telephone"`

	Security *Security     `bson:"security"           json:"-"`
	Info     *PersonalInfo `bson:"personalInfo"       json:"personalInfo"`
}

func NewUser() *User {
	return &User{
		Id:       bson.NewObjectId(),
		UID:      uuid.NewRandom().String(),
		Security: NewSecurity(),
		Info:     NewEmptyPersonalInfo(),
	}
}
