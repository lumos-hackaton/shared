package user

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"github.com/labstack/gommon/random"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type (
	Security struct {
		EncodedPassword    []byte    `bson:"encodedPassword"     json:"-"`
		LastPasswordChange time.Time `bson:"lastPasswordChange"  json:"-"`
		Secret             string    `bson:"secret"              json:"-"`

		IsEmailVerified bool `bson:"emailVerified"       json:"emailVerified"`
		IsEnabled       bool `bson:"enabled"             json:"enabled"`

		IsUsing2FA bool            `bson:"using2FA"            json:"using2FA"`
		LastTokens []bson.ObjectId `bson:"lastTokens"          json:"-"`

		DevicesIds []*dbase.DBRef `bson:"devicesId"           json:"-"`
	}

	OldPassword struct {
		UserId       *dbase.DBRef `bson:"userId"`
		PasswordHash []byte       `bson:"passwordHash"`
		ChangesFrom  int64        `bson:"fromTime"`
		ChangedOn    int64        `bson:"toTime"`
	}

	Token struct {
		Id       bson.ObjectId `bson:"_id"                json:"-"`
		UserId   *dbase.DBRef  `bson:"ownerId"            json:"-"`
		DeviceId *dbase.DBRef  `bson:"deviceId"           json:"-"`

		Hash         string        `bson:"hash"               json:"hash"`
		StartedAt    time.Time     `bson:"startedAt"          json:"-"`
		LiveCycle    time.Duration `bson:"liveCycle"          json:"-"`
		RefreshToken *RefreshToken `bson:"refreshToken"       json:"refreshToken"`
	}

	RefreshToken struct {
		Hash      string    `bson:"hash"               json:"hash"`
		StartedAt time.Time `bson:"startedAt"          json:"-"`
		SSCode    string    `bson:"sscode"             json:"-"`
		IsActive  bool      `bson:"isActive"           json:"-"`
	}
)

func NewSecurity() *Security {
	return &Security{
		Secret: Random(),
	}
}

func NewToken() *Token {
	return &Token{
		Id: bson.NewObjectId(),
	}
}

// utils

func Random() string {
	//srt := rand.String(2, random.Numeric)
	//c, _ := strconv.ParseUint(srt, 36, 63)
	count := 10 // c % 10 + 10
	return random.New().String(uint8(count), random.Alphanumeric)
}
