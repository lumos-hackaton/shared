package user

import (
	"bitbucket.org/lumos-hackaton/shared/configs"
)

type (
	PersonalInfo struct {
		Name    string `json:"name"                bson:"name"          validate:"omitempty,gte=3,lte=128"`
		Surname string `json:"surname"             bson:"surname"       validate:"omitempty,gte=3,lte=128"`

		PhotoUrl string `json:"photo"               bson:"photo"         validate:"-"`
		Phone    string `json:"telephone,omitempty" bson:"telephone"     validate:"omitempty"`

		Referral     string `json:"-"                   bson:"referral"      validate:"-"`
		ReferralType string `json:"-"                   bson:"referralType"  validate:"-"`
		Language     string `json:"language,omitempty"  bson:"language"      validate:"omitempty"`
	}
)

func userImage() string {
	return configs.Instance().Env.Backend + "/img/default/user.png"
}


func NewEmptyPersonalInfo() *PersonalInfo {
	return &PersonalInfo{
		PhotoUrl: userImage(),
	}
}
