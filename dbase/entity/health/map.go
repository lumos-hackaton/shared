package health

import "gopkg.in/mgo.v2/bson"

type DataMap struct {
	Id        bson.ObjectId   `bson:"_id"`

	Region    string          `bson:"region"`
	Timestamp int64           `bson:"timestamp"`
	Data      DataValue     `bson:"data"`
}

type DataValue struct {
	Temperature    *Temperature    `bson:"temperature"`
	MoonCycle      *MoonCycle      `bson:"moon"`
	Ecology        *Ecology        `bson:"ecology"`
	SunInfluence   *SunInfluence   `bson:"sun"`
}


type SunInfluence struct {
	MagneticStorms  interface{} `bson:"magneticStorms"`
}

type MoonCycle struct {
	Percents   float32  `bson:"percents"`
}

type Ecology struct {
	AirPollution           float64 `bson:"airPollution"`
	RadioactivePollution   float64 `bson:"radioactivePollution"`
}


type Temperature struct {
	Air      float64   `bson:"air"`
	Earth    float64   `bson:"earth"`
	Pressure float64   `bson:"pressure"`
}
