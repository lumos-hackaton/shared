package health

import "gopkg.in/mgo.v2/bson"

type User struct {
	Id              bson.ObjectId `bson:"_id"`
	Uid             string        `bson:"uid"`

	Email           string        `bson:"email"`
	Name            string        `bson:"name"`
	Surname         string        `bson:"surname"`

	DateOfBirthday  int64         `bson:"birthday"`
}