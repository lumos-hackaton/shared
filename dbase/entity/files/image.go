package files

import (
	"github.com/pborman/uuid"
	"bitbucket.org/foodex-backend/protobuf/generated"
)

type SimpleImage struct {
	generated.File
}

func NewImage(uid string, data []byte, ext string) *SimpleImage {
	return &SimpleImage{
		File: generated.File{
			Filename: uid + uuid.NewRandom().String(),
			ContentType: ext,
			Data: data,
		},
	}
}