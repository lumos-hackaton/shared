package dbase

type Query interface {
	Skip(n int) Query
	Limit(n int) Query

	Select(selector []string) Query
	Sort(fields ...string) Query

	One(result interface{}) error
	All(result interface{}) error
	Count() (n int, err error)
	Exists() (bool, error)
}