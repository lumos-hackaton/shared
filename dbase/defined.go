package dbase

type (
	authCollection        string
	usersCollection       string
	groupsCollection      string
	chatsCollection       string
	marketCollection      string
	statisticsCollection  string

	collections struct {
		Auth        authCollection
		Users       usersCollection
		Groups      groupsCollection
		Chats       chatsCollection
		Market      marketCollection
		Statistics  statisticsCollection
	}
)

var (
	Auth = authCollection("auth")
	Users = usersCollection("users")
	Chats = chatsCollection("messaging")
	Statistics = statisticsCollection("statistics")

	emptyDB Database = &tempDb{}
)


// User
func (c usersCollection) User(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c))
}
func (c usersCollection) Security(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".security")
}
func (c usersCollection) Devices(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".devices")
}
func (c usersCollection) Locations(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".locations")
}




// Auth
func (c authCollection) OldPasswords(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".oldPassword")
}
func (c authCollection) Tokens(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".loginTokens")
}
func (c authCollection) EmailVerificationTokens(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".emailVerification")
}




// GroupByUser
func (c groupsCollection) Groups(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c))
}
func (c groupsCollection) Relations(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".relations")
}
func (c groupsCollection) Roles(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".roles")
}




// Market
func (c marketCollection) SubmitOrders(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".submitOrders")
}
func (c marketCollection) Orders(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".orders")
}
func (c marketCollection) OrdersHistory(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".ordersHistory")
}
func (c marketCollection) ComplexOrders(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".complexOrders")
}
func (c marketCollection) Products(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".products")
}
func (c marketCollection) OrderTemplates(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".orderTemplates")
}




// Chat
func (c chatsCollection) PersonalMessages(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".personalMessages")
}
func (c chatsCollection) OrderMessages(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".orderMessages")
}
func (c chatsCollection) CompanyMessages(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) +  ".companyMessages")
}
func (c chatsCollection) Company(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".companyChat")
}
func (c chatsCollection) Order(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".orderChat")
}
func (c chatsCollection) Personal(db Database) Collection {
	if db == nil { db = emptyDB }
	return db.Collection(string(c) + ".personalChat")
}