package dbase

type (
	M map[string]interface{}
	D []E

	E struct {
		Name  string
		Value interface{}
	}

	DBRef struct {
		Database string
		Collection string
		Id interface{}
	}
)